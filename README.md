# XDG FileChooser portal implementation

This is a testbed for a new file chooser widget for GTK, available
as an implementation of the [XDG FileChooser portal
interface][interface].

For implementation notes, see [notes.org].





[interface]: https://github.com/flatpak/xdg-desktop-portal/blob/master/data/org.freedesktop.impl.portal.FileChooser.xml
[default-impl]: https://github.com/flatpak/xdg-desktop-portal-gtk
[notes.org]: doc/notes.org

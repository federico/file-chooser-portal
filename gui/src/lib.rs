use url::Url;

mod dialog;

pub enum Action {
    Open,
    Save,
}

pub struct Setup {
    action: Action,
    modal: bool,
    parent_window_id: String,
    title: String,
    accept_label: Option<String>,
}

pub struct DialogResult {
    pub response: u32,
    pub uris: Vec<Url>,
    pub choices: Vec<(String, String)>,
}

pub fn dialog(
    setup: Setup,
    options: (), // FIXME
) -> DialogResult {
    unimplemented!()
}

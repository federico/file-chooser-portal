use gtk::{self, prelude::*};

use crate::{Action, Setup};

enum FileView {
    List(gtk::ListBox),
    Icon(gtk::IconView),
}

impl Action {
    fn default_accept_label(&self) -> String {
        match self {
            Action::Open => "_Open".to_string(),
            Action::Save => "_Save".to_string(),
        }
    }
}

struct Dialog {
    window: gtk::Dialog,
    //    header: gtk::HeaderBar,

    //    sidebar: gtk::PlacesSidebar,
    //    files: FileView,
    //    entry: gtk::Entry,
    //    path_bar: gtk::Button, // FIXME
    //    new_folder: gtk::Button,

    //    sidebar_toggle: gtk::ToggleButton,
    //    list_radio: gtk::RadioButton,
    //    icon_radio: gtk::RadioButton,

    //    cancel: gtk::Button,
    //    accept: gtk::Button,
}

impl Dialog {
    fn from_setup(setup: &Setup) -> Dialog {
        let mut flags = gtk::DialogFlags::USE_HEADER_BAR;
        if setup.modal {
            flags |= gtk::DialogFlags::MODAL;
        }

        // FIXME: figure out the parent window
        let parent = None::<gtk::Window>;

        let cancel_label = "Cancel";
        let accept_label = &setup
            .accept_label
            .clone()
            .unwrap_or_else(|| setup.action.default_accept_label());

        let window = gtk::Dialog::new_with_buttons(
            Some(&setup.title),
            parent.as_ref(),
            flags,
            &vec![
                (cancel_label, gtk::ResponseType::Cancel),
                (accept_label, gtk::ResponseType::Accept),
            ],
        );

        Dialog { window }
    }
}

enum ViewKind {
    List,
    Icon,
}

enum Panes {
    Sidebar,
    Files,
    Both,
}

struct Model {
    panes: Panes,
    view_kind: ViewKind,
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::sync::Once;

    fn gtk_init() {
        static INIT: Once = Once::new();

        INIT.call_once(|| {
            gtk::init().expect("gtk::init");
        });
    }

    fn check_dialog_has_setup_parameters(setup: &Setup) {
        let dialog = Dialog::from_setup(setup);

        assert_eq!(dialog.window.get_modal(), setup.modal);
        assert_eq!(
            dialog
                .window
                .get_title()
                .expect("window has title")
                .as_str(),
            setup.title
        );

        assert!(dialog
            .window
            .get_widget_for_response(gtk::ResponseType::Cancel)
            .is_some());

        let button: gtk::Button = dialog
            .window
            .get_widget_for_response(gtk::ResponseType::Accept)
            .expect("widget with Accept response exists")
            .downcast()
            .expect("widget with Accept response is a Button");

        assert_eq!(
            button
                .get_label()
                .expect("Accept button has label")
                .as_str(),
            setup
                .accept_label
                .clone()
                .unwrap_or_else(|| setup.action.default_accept_label()),
        );
    }

    fn uses_provided_accept_label() {
        let setup = Setup {
            action: Action::Open,
            modal: true,
            parent_window_id: "".to_string(),
            title: "Hello world".to_string(),
            accept_label: Some("Foo".to_string()),
        };

        check_dialog_has_setup_parameters(&setup);
    }

    fn provides_default_accept_label() {
        let mut setup = Setup {
            action: Action::Open,
            modal: true,
            parent_window_id: "".to_string(),
            title: "Hello world".to_string(),
            accept_label: None,
        };

        check_dialog_has_setup_parameters(&setup);

        setup.action = Action::Save;
        check_dialog_has_setup_parameters(&setup);
    }

    #[test]
    fn all_tests() {
        // GTK can only be called from a single thread, so we must
        // call each test sequentially :(
        //
        // See https://github.com/rust-lang/rust/issues/43155
        //
        // As of 2019/Nov/06 we can't switch to using
        // https://github.com/palfrey/serial_test/ because although it
        // runs tests serially, each test happens in a different
        // thread (it uses a mutex around each test) and gtk-rs
        // rightfully complains that it is not being called from the
        // same thread.

        gtk_init();

        uses_provided_accept_label();
        provides_default_accept_label();
    }
}

//! Idiomatic API for the file chooser.
//!
//! This is a Rust-friendly API for the file chooser.  At runtime, the incoming arguments
//! from DBus calls get converted into types from this module.
//!
//! See the [`dbus_api`] module for the corresponding types as they come in from the
//! initial DBus deserialization.
//!
//! The DBus API has a little bunch of argument types that are built of variant
//! primitives, and which are meant to be interpreted according to the [documentation] of
//! the `org.freedesktop.impl.portal.FileChooser` interface.  For example, an empty array
//! of some elements may mean something, while a nonempty array means something else.
//! This modules validates those arguments and converts them to Rust rich types.
//!
//! [documentation]: https://github.com/flatpak/xdg-desktop-portal/blob/main/data/org.freedesktop.impl.portal.FileChooser.xml

use crate::dbus_api;

struct OpenFileOptions {
    accept_label: String,
    modal: bool,
    multiple: bool,
    directory: bool,
    filters: Vec<Filter>,
    current_filter: Option<Filter>,
    choices: Vec<Choice>,
}

#[derive(Debug, PartialEq)]
struct Filter {
    human_readable_name: String,
    specs: Vec<FilterSpec>,
}

#[derive(Debug, PartialEq)]
enum FilterSpec {
    Glob(String),
    Mime(String),
}

#[derive(Debug, PartialEq)]
struct Choice {
    id: String,
    human_readable_label: String,
    kind: ChoiceKind,
}

#[derive(Debug, PartialEq)]
enum ChoiceKind {
    Combo {
        items: Vec<ComboChoiceItem>,
        initial_selection_idx: Option<usize>,
    },

    Checkbox {
        checked: bool,
    },
}

#[derive(Debug, PartialEq)]
struct ComboChoiceItem {
    id: String,
    human_readable_label: String,
}

macro_rules! s {
    ($e:expr) => {
        $e.to_string()
    };
}

impl OpenFileOptions {
    pub fn from_dbus(options: dbus_api::OpenFileOptions) -> Result<Self, String> {
        let dbus_api::OpenFileOptions {
            accept_label,
            modal,
            multiple,
            directory,
            filters,
            current_filter,
            choices,
        } = options;

        // The following define the defaults in case an option is not specified in the DBus call.
        // The defaults come from the documentation for org.freedesktop.impl.portal.FileChooser
        let modal = modal.unwrap_or(true);
        let multiple = multiple.unwrap_or(false);
        let directory = directory.unwrap_or(false);

        let filters = match filters {
            None => Vec::new(),
            Some(filters) => filters
                .into_iter()
                .map(|filter| Filter::from_dbus(filter))
                .collect::<Result<Vec<Filter>, String>>()?,
        };

        let current_filter = match current_filter {
            None => None,
            Some(filter) => Some(Filter::from_dbus(filter)?),
        };

        let choices = match choices {
            None => Vec::new(),
            Some(choices) => choices
                .into_iter()
                .map(|choice| Choice::from_dbus(choice))
                .collect::<Result<Vec<Choice>, String>>()?,
        };

        let accept_label = match accept_label {
            // These come from xdg-desktop-portal-gnome/src/filechooser.c
            None => {
                // FIXME: localize
                if multiple {
                    s!("_Open")
                } else {
                    s!("_Save")
                }
            }
            Some(label) => label,
        };

        Ok(OpenFileOptions {
            accept_label,
            modal,
            multiple,
            directory,
            filters,
            current_filter,
            choices,
        })
    }
}

impl Filter {
    fn from_dbus(f: dbus_api::Filter) -> Result<Self, String> {
        let dbus_api::Filter {
            human_readable_name,
            specs,
        } = f;

        let specs: Vec<FilterSpec> = specs
            .into_iter()
            .map(|spec| FilterSpec::from_dbus(spec))
            .collect::<Result<Vec<FilterSpec>, String>>()?;
        Ok(Filter {
            human_readable_name,
            specs,
        })
    }
}

impl FilterSpec {
    fn from_dbus(spec: dbus_api::FilterSpec) -> Result<Self, String> {
        match spec {
            dbus_api::FilterSpec { kind: 0, pattern } => Ok(FilterSpec::Glob(pattern)),
            dbus_api::FilterSpec { kind: 1, pattern } => Ok(FilterSpec::Mime(pattern)),
            dbus_api::FilterSpec { kind, .. } => Err(format!("invalid filter kind {}", kind)),
        }
    }
}

impl Choice {
    fn from_dbus(choice: dbus_api::Choice) -> Result<Choice, String> {
        let dbus_api::Choice {
            id,
            human_readable_label,
            items,
            initial_selection_id,
        } = choice;

        let kind = if items.is_empty() {
            let checked = match initial_selection_id.as_ref() {
                "false" => false,
                "true" => true,
                _ => {
                    return Err(format!(
                        "invalid initial value for checkbox choice: '{}'",
                        initial_selection_id
                    ))
                }
            };
            ChoiceKind::Checkbox { checked }
        } else {
            let items = items
                .into_iter()
                .map(|item| ComboChoiceItem::from_dbus(item))
                .collect::<Vec<ComboChoiceItem>>();

            // Find the initial_selection_id in the items.
            // If it is "", then it means that there is no initial selection.
            let initial_selection_idx = match initial_selection_id.as_ref() {
                "" => None,
                id => {
                    let pos = items
                        .iter()
                        .position(|item| item.id == initial_selection_id);
                    match pos {
                        None => return Err(format!("selection id '{}' not found in choices", id)),
                        Some(x) => Some(x),
                    }
                }
            };

            ChoiceKind::Combo {
                items,
                initial_selection_idx,
            }
        };

        Ok(Choice {
            id,
            human_readable_label,
            kind,
        })
    }
}

impl ComboChoiceItem {
    fn from_dbus(item: dbus_api::ChoiceItem) -> Self {
        ComboChoiceItem {
            id: item.id,
            human_readable_label: item.human_readable_label,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn make_dbus_choice(initial_selection_id: String) -> dbus_api::Choice {
        dbus_api::Choice {
            id: s!("foo"),
            human_readable_label: s!("Foo"),
            items: vec![
                dbus_api::ChoiceItem {
                    id: s!("0"),
                    human_readable_label: s!("Zero"),
                },
                dbus_api::ChoiceItem {
                    id: s!("1"),
                    human_readable_label: s!("One"),
                },
            ],
            initial_selection_id,
        }
    }

    fn converts_combo_choice(initial_selection_id: String, initial_selection_idx: Option<usize>) {
        let dbus_choice = make_dbus_choice(initial_selection_id);

        let choice = Choice {
            id: s!("foo"),
            human_readable_label: s!("Foo"),
            kind: ChoiceKind::Combo {
                items: vec![
                    ComboChoiceItem {
                        id: s!("0"),
                        human_readable_label: s!("Zero"),
                    },
                    ComboChoiceItem {
                        id: s!("1"),
                        human_readable_label: s!("One"),
                    },
                ],
                initial_selection_idx,
            },
        };

        assert_eq!(Choice::from_dbus(dbus_choice).unwrap(), choice);
    }

    #[test]
    fn converts_combo_choice_with_no_initial_selection() {
        converts_combo_choice(s!(""), None);
    }

    #[test]
    fn converts_combo_choice_with_initial_selection() {
        converts_combo_choice(s!("1"), Some(1));
    }

    #[test]
    fn detects_combo_choice_with_invalid_initial_selection() {
        let dbus_choice = make_dbus_choice(s!("blah"));
        assert!(Choice::from_dbus(dbus_choice).is_err());
    }

    fn make_dbus_checkbox_choice(initial_selection_id: String) -> dbus_api::Choice {
        dbus_api::Choice {
            id: s!("foo"),
            human_readable_label: s!("Foo"),
            items: Vec::new(),
            initial_selection_id,
        }
    }

    fn make_checkbox_choice(checked: bool) -> Choice {
        Choice {
            id: s!("foo"),
            human_readable_label: s!("Foo"),
            kind: ChoiceKind::Checkbox { checked },
        }
    }

    #[test]
    fn converts_checkbox_choice() {
        assert_eq!(
            Choice::from_dbus(make_dbus_checkbox_choice(s!("false"))).unwrap(),
            make_checkbox_choice(false)
        );
        assert_eq!(
            Choice::from_dbus(make_dbus_checkbox_choice(s!("true"))).unwrap(),
            make_checkbox_choice(true)
        );
    }

    #[test]
    fn detects_checked_choice_with_invalid_initial_selection() {
        assert!(Choice::from_dbus(make_dbus_checkbox_choice(s!("not_a_boolean_value"))).is_err());
    }

    #[test]
    fn converts_filter() {
        let dbus_filter = dbus_api::Filter {
            human_readable_name: s!("Foo"),
            specs: vec![
                dbus_api::FilterSpec {
                    kind: 0,
                    pattern: s!("myglob*"),
                },
                dbus_api::FilterSpec {
                    kind: 1,
                    pattern: s!("x-application/my-mime"),
                },
            ],
        };

        let filter = Filter {
            human_readable_name: s!("Foo"),
            specs: vec![
                FilterSpec::Glob(s!("myglob*")),
                FilterSpec::Mime(s!("x-application/my-mime")),
            ],
        };

        assert_eq!(Filter::from_dbus(dbus_filter).unwrap(), filter);
    }

    #[test]
    fn detects_filter_with_invalid_kind() {
        let dbus_filter = dbus_api::Filter {
            human_readable_name: s!("Foo"),
            specs: vec![dbus_api::FilterSpec {
                kind: 42,
                pattern: s!("plonk"),
            }],
        };

        assert!(Filter::from_dbus(dbus_filter).is_err());
    }
}

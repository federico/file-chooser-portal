//! Types for the various arguments in the DBus API.
//!
//! These come from the initial deserialization of DBus variants into Rust types, and
//! consist mostly of primitive types.
//!
//! The values must then be validated and interpreted according to the [documentation] of
//! the `org.freedesktop.impl.portal.FileChooser` interface.  See the [`crate::api`]
//! module for that.
//!
//! [documentation]: https://github.com/flatpak/xdg-desktop-portal/blob/main/data/org.freedesktop.impl.portal.FileChooser.xml

/// Values from the `options` argument in the `OpenFile` method, which comes in
/// as a `a{sv}`.
pub struct OpenFileOptions {
    pub accept_label: Option<String>,
    pub modal: Option<bool>,
    pub multiple: Option<bool>,
    pub directory: Option<bool>,
    pub filters: Option<Vec<Filter>>,
    pub current_filter: Option<Filter>,
    pub choices: Option<Vec<Choice>>,
}

// `(sa(us))`
pub struct Filter {
    pub human_readable_name: String,
    pub specs: Vec<FilterSpec>,
}

// `(us)`
pub struct FilterSpec {
    /// glob:0, mimetype:1
    pub kind: u32,
    pub pattern: String,
}

/// A combo or checkbox "choice".
///
/// Variant signature is `(ssa(ss)s)`.
///
/// * If the items array is empty, then this is a boolean choice and initial_selection_id
/// will be `true` or `false`.
///
/// * If the items array is not empty, then it has items for a combo box or similar.  The
/// initial_selection_id corresponds to the [`ChoiceItem.id`] of the initially-selected
/// item, or "" to let the portal decide which one to select initially.
pub struct Choice {
    pub id: String,
    pub human_readable_label: String,
    pub items: Vec<ChoiceItem>,
    pub initial_selection_id: String,
}

// `(ss)`
pub struct ChoiceItem {
    pub id: String,
    pub human_readable_label: String,
}
